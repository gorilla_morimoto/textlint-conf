# README

2018-05-08

## セットアップ

1. このリポジトリを`~/opt`にクローン
2. `npm install`で必要なパッケージをインストール
3. `./bin/install.sh`を実行して`~/bin/`に`textlint`の実行スクリプトを置く

## 実行

任意のディレクトリで以下のコマンドを実行する。

```sh
$ textlint <file-name>

```

textlint自体はローカルインストールだがスクリプトにパスを通しているので、これで実行できる。スクリプトの中身は以下の通り。

```sh
#!/usr/bin/env bash
~/opt/textlint/node_modules/.bin/textlint -c ~/opt/textlint/.textlintrc "$@"

```

## ルールを追加する

### 置換

- prhパッケージを使用する
- ルールは`yml`ファイルに記述し、`.textlintrc`の`"prh.rulePaths"`に追加する
- 書式は`./prh-rules.yml`に記載されているURLを参照


## 使用しているパッケージ

### textlint-rule-prh

[GitHub](https://github.com/textlint-rule/textlint-rule-prh)

- 文字列を置換するパッケージ

- 設定ファイルは`./prh-rules/main.yml`
  - 詳細は[こちら](https://github.com/textlint-rule/textlint-rule-prh)が詳しい


### textlint-rule-preset-ja-technical-writing

[GitHub](https://github.com/textlint-ja/textlint-rule-preset-ja-technical-writing)

- さまざまなパッケージをひとまとめにしてくれている

- 個別のパッケージのオンオフにも対応している
	- `.textlint`を参照
	- [この方のブログ](http://neos21.hatenablog.com/entry/2017/11/05/0800000)も参考になる

### textlint-plugin-html

[GitHub](https://github.com/textlint/textlint-plugin-html)

- HTMLファイルにもリントを適用してくれるプラグイン

### textlint-rule-preset-JTF-style

[GitHub](https://github.com/textlint-ja/textlint-rule-preset-JTF-style)

- JTFの日本語標準スタイルガイドに準拠したルール


